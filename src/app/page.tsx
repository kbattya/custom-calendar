import Image from "next/image";
import Sceduler from "./client/scheduler/indnex";

import { schedules } from '../app/data/schedules.json';

export const revalidate = 5;

export default function Home() {
  return (
    <main>
			<Sceduler schedules={schedules}/>
    </main>
  );
}
