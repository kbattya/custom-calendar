export const ArrowLeft = () => {
	return (
		<svg width="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M6.91667 12.3333L1.08334 6.50001L6.91667 0.666678" stroke="currentColor" strokeWidth="0.999833" strokeLinecap="round" strokeLinejoin="round"/>
		</svg>
	)
}