export interface IDate {
	day: number,
	month: number,
	date: string,
	slots: TimeSlot[]
}

export interface TimeSlot {
	duration: number,
	price: number,
	id: string,
	instructorIdid: string,
	organizationId: string,
	startTime: string,
	title: string,
	// timeslots: string[]
}

export interface ScheduledEventPayload {
  orgId: string
  instructorId?: string
  serviceId?: string
  dateBegin?: string
  dateEnd?: string
  packageId?: string
}

export interface Root {
  schedules: Schedule[]
}

export interface Schedule {
  eventSlotDto: EventSlotDto
  serviceDto: ServiceDto
  instructorDto: InstructorDto
}

export interface EventSlotDto {
  id: string
  startTime: string
  endTime: string
  availableSpots: number
  organizationId: string
  serviceId: string
  instructorId: string
  eventReference: string
  duration: number
  hasPaymentEnabled: boolean
  eventType: string
  totalSpots: number
  price: number
  repeatType: string
  startDate: string
  endEndDate: string
  timeslots: any[]
  repeatedDays: any[]
}

export interface ServiceDto {
  id: string
  name: string
  description: string
  addressStreetNumber: any
  addressStreetName: string
  addressCity: string
  addressCountry: string
  featuredImage: string
  currency: any
  images: string[]
  isActive: boolean
  organizationId: string
  rating: any
  tags: any[]
}

export interface InstructorDto {
  organizationId: string
  rating: any
  bio: string
  services: any[]
  certificates: any
  languages: any
  specialties: string[]
  categories: string[]
  title: string
  yearsOfExperience: number
  createdAt: any
  id: string
  password: any
  email: string
  resetPasswordToken: any
  firstName: string
  lastName: string
  isActivated: boolean
  archived: boolean
  isTrusted: boolean
  roles: any
  profilePicture: string
  contact: any
}
