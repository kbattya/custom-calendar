import { IDate } from "../models/models";

export  function dateToEpoch(thedate:Date) {
	var time = thedate.getTime();
	return time - (time % 86400000);
}
