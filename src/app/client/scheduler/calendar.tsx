'use client'
import React, { useEffect, useState } from "react";
import { ArrowLeft } from "../../icons";

import { IDate, Schedule, TimeSlot } from "../../models/models";	
import WeekView from "./weekCalendar";

interface Props {
	schedules: Schedule[];
	selectedDay: IDate | null;
	setSelectedDay: (day: IDate | null) => void;
}

const Calendar: React.FC<Props> = ({schedules, selectedDay, setSelectedDay}) => {
	const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];

	const [isMonthMode, setIsMonthMode] = useState(false)
	const [selectedMonth, setSelectedMonth] = useState<any>()
	const [selectedWeek, setSelectedWeek] = useState<any>()

	useEffect(() => {
		selectedMonth?.length > 0 && console.log(month[selectedMonth[0][6].month])
	}, [selectedMonth])

	useEffect(() => {
		const currentWeek = getCurrentWeekDates();
		setSelectedWeek(currentWeek)
		setSelectedMonth(getCurrentMonth())
	}, [])
	
	const getPreviousWeekDates = (baseDate = new Date()) => getWeekDates(baseDate, -7);
  const getNextWeekDates = (baseDate = new Date()) => getWeekDates(baseDate, 7);
  const getCurrentWeekDates = (baseDate = new Date()) => getWeekDates(baseDate, 0);

	const getWeekDates = (baseDate: Date, offset: number): IDate[] => {
    const date = new Date(baseDate);
    date.setDate(date.getDate() + offset - (date.getDay() === 0 ? 6 : date.getDay() - 1));
    
    return Array.from({ length: 7 }).map((_, i) => {
      const currentDate = new Date(date);
      currentDate.setDate(date.getDate() + i);
      return {
        day: currentDate.getUTCDate(),
        month: currentDate.getUTCMonth(),
        date: currentDate.toISOString().split('T')[0],
        slots: getTimeSlots(currentDate),
      };
    });
  };

	function getWeeksForMonth(year: number, month: number) {
    const weeks = [];
    let week = [];

    const firstDayOfMonth = new Date(year, month, 1);
    const lastDayOfMonth = new Date(year, month + 1, 0);

    let firstDayOfWeek = firstDayOfMonth.getDay();
    const daysInMonth = lastDayOfMonth.getDate();

    if (firstDayOfWeek === 0) {
        firstDayOfWeek = 6; // If it's Sunday, shift to 6
    } else {
        firstDayOfWeek--; // Otherwise, shift back one day
    }

    for (let i = 0; i < firstDayOfWeek; i++) {
        week.push(null);
    }

    for (let day = 1; day <= daysInMonth; day++) {
				const currentDate = new Date(Date.UTC(year, month, day));
        week.push({
            day: currentDate.getUTCDate(),
            month: currentDate.getMonth(),
            date: currentDate.toISOString(),
            slots: getTimeSlots(currentDate),
        });

        if (week.length === 7) {
            weeks.push(week);
            week = [];
        }
    }

    if (week.length > 0) {
        while (week.length < 7) {
            week.push(null);
        }
        weeks.push(week);
    }

    return weeks;	
	}

	function getCurrentMonth() {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    return getWeeksForMonth(year, month);
	}

	function getNextMonth() {
    const today = new Date();
    const year = today.getFullYear();
    const month = selectedMonth[0][6].month + 1;
    return getWeeksForMonth(year, month);
	}

	function getPrevMonth() {
    const today = new Date();
    const year = today.getFullYear();
    const month = selectedMonth[0][6].month - 1;
    return getWeeksForMonth(year, month);
	}

	const getTimeSlots = (date:Date) => {
		let todayEvents = schedules.filter((scedule:Schedule) => scedule.eventSlotDto.startDate === date.toISOString().substring(0, 10))
		let slots = todayEvents.map((item:Schedule):TimeSlot => {
			return ({
				duration: item.eventSlotDto.duration,
				price: item.eventSlotDto.price,
				id: item.eventSlotDto.id,
				instructorIdid: item.eventSlotDto.instructorId,
				organizationId: item.eventSlotDto.organizationId,
				startTime: item.eventSlotDto.startTime,
				// ? event title
				title: item.instructorDto.title,
			})
		})
		return slots
	}


 return (
	<>
		{!isMonthMode &&
			<div className="flex items-center justify-center">
				<div>{selectedWeek?.length > 0 && month[selectedWeek[3].month]}</div>
			</div>
		}

		<div className="btn_group"> 
			<button
				className={`btn_group_item ${isMonthMode ? '' : 'btn_group_item--active'}`}
				onClick={() => setIsMonthMode(false)}
			>
				Week
			</button>
			<button
				className={`btn_group_item ${isMonthMode ? 'btn_group_item--active' : ''}`}
				onClick={() => setIsMonthMode(true)}
			>
				Month
			</button>
		</div>

		{isMonthMode &&
			<div className="flex flex-row justify-between">
				<div>
					<button
						className="flex"
						onClick={() => setSelectedMonth(getPrevMonth())}
					>
						<ArrowLeft />
					</button>
				</div>
				<div>{selectedMonth?.length > 0 && month[selectedMonth[0][6].month]}</div>
				
				<div>
					<button
						className="flex"
						style={{transform: 'rotate(180deg'}}
						onClick={() => setSelectedMonth(getNextMonth())}
					>
						<ArrowLeft />
					</button>
				</div>
			</div>
		}
		<table className="border-collapse w-[300px] m-auto">
			<thead>
				<tr>
					<th>
						{!isMonthMode && 
							<button
								className="flex"
								onClick={() => setSelectedWeek(getPreviousWeekDates(new Date(selectedWeek[0].date)))}
							>
								<ArrowLeft />
							</button>
						}
						
					</th>
					
					<th>Mo</th>
					<th>Tu</th>
					<th>We</th>
					<th>Tr</th>
					<th>Fr</th>
					<th>St</th>
					<th>Su</th>

					<th>
						{!isMonthMode && 
							<button
								className="icon_btn"
								style={{transform: 'rotate(180deg'}}
								onClick={() => setSelectedWeek(getNextWeekDates(new Date(selectedWeek[0].date)))}
							>
								<ArrowLeft />
							</button>
						}
					</th>
				</tr>
			</thead>

			<tbody className="cursor-pointer">
				{isMonthMode
					? <>
						{selectedMonth.map((week:any, index:number) => {
							return (
								<WeekView
									key={index}
									week={week}
									onHandleDateSelect={(date:IDate) => setSelectedDay(date)}
									selectedDay={selectedDay}
								/>
							)
						})}
							
						</>
					: <WeekView
							week={selectedWeek}
							onHandleDateSelect={(date:IDate) => setSelectedDay(date)}
							selectedDay={selectedDay}
						/>
				}
			</tbody>

		</table>
	</>
 )
}

export default Calendar
