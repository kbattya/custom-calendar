'use client'
import React, { useState } from "react";
import Image from "next/image";
import { ArrowLeft } from "../../icons";

import { IDate, Root, TimeSlot } from "../../models/models";	
import Calendar from "./calendar";
import TimeSlots from "./timeSlots";


const Sceduler = ({schedules}:Root) => {
	const [selectedDay, setSelectedDay] = useState<IDate | null>()
	const [selectedSlot, setSelectedSlot] = useState<TimeSlot | null>()

 return (
	<div className="chat_wrapper fixed right-[5vw] bottom-[5vw] w-screen max-w-[400px] flex flex-col gap-[20px] border-[1px] border-[solid] border-[#E5E6EB]">
		<header className="chat_header flex flex-row justify-between pt-[22px] px-[16px] pb-[12px] border-b-[1px] border-b-[solid] border-b-[#E5E6EB] bg-[white]">
			<Image
				priority
				src="/images/leefstylist_logo1.svg"
				height={40}
				width={127}
				quality={100} 
				alt="Follow us on Twitter"
			/>

			<button className="btn_oulined">
				Account
			</button>
		</header>

		<div className={`flex flex-col gap-[20px] p-[20px] [@media(max-width:320px)]:p-[10px] ${selectedSlot ? '[@media(max-width:425px)]:pb-[140px]' : ''}`}>
			<Calendar
				schedules={schedules}
				selectedDay={selectedDay ?? null}
				setSelectedDay={setSelectedDay}
			/>

			{selectedDay &&
				<TimeSlots
					selectedDay={selectedDay}
					selectedSlot={selectedSlot ?? null}
					setSelectedSlot={setSelectedSlot}
				/>
			}
		</div>

		{selectedSlot &&
			<div className="checkout_link">
				<div className="flex flex-col gap-[12px]">
					<div className="flex flex-row items-center break-all">
						{selectedSlot.title}
						<button className="-rotate-90">
							<ArrowLeft />
						</button>
					</div>
					<div className="font-semibold text-[#6C727F]">&#8364;{selectedSlot.price}</div>
				</div>
				<button className="btn_primary">
					Checkout
				</button>
			</div>
		}
	</div>
 )
}

export default Sceduler
