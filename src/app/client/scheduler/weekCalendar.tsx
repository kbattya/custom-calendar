import { useEffect } from "react";
import { dateToEpoch } from "../../helpers/helpers"
import { IDate } from "@/app/models/models";
const { v4: uuidv4 } = require('uuid');

interface Props {
	week: [IDate, ...(IDate | null)[]];
	onHandleDateSelect: (day: IDate) => void;
	selectedDay:  IDate | null;
}

const WeekView: React.FC<Props>  = ({week, onHandleDateSelect, selectedDay}) => {

	const isToday = (date:string):boolean => {
		return dateToEpoch(new Date()) === dateToEpoch(new Date(date))
	}

	const isSelected = (date:string):boolean => {
		if (typeof selectedDay?.date ===  "string") {
			return dateToEpoch(new Date(date)) === dateToEpoch(new Date(selectedDay.date))
		} else {
			return false
		}
	}

	const isDisabled = (date:string):boolean => {
		return dateToEpoch(new Date()) > dateToEpoch(new Date(date))
	}

	return (
		<tr>
			<td></td>
				{week?.length > 0 && week.map((item:IDate | null) => {
					return (
						<td
							key={item?.day || uuidv4()}
							onClick={() => item && !isDisabled(item.date) && onHandleDateSelect(item)}
						>
							{item !== null
								?	<>
										<div
											className={
												`calendar_day_item ${isToday(item.date) ? 'calendar_day_item--today ' : ''}`+
												`${isSelected(item.date) ? 'calendar_day_item--selected ' : ''}`+
												`${isDisabled(item.date) ? 'calendar_day_item--disabled ' : ''}`
											}
											>
											{item.day}
										</div>
										<div className={`mx-[auto] my-[2px] w-[3px] h-[3px] rounded-[50%] ${item.slots.length > 0 ? 'bg-[#007AFF]' : ''}`}></div>
									</>
								: <></>
							}
						</td>
					)
				})}
			<td></td>
		</tr>
	)
}


export default WeekView