'use client'

import { TimeSlot } from "@/app/models/models"
import { IDate } from "@/app/models/models";

interface Props {
	selectedDay: IDate | null;
	selectedSlot: TimeSlot | null;
	setSelectedSlot: (day: TimeSlot | null) => void;
}

const TimeSlots: React.FC<Props> = ({selectedDay, selectedSlot, setSelectedSlot}) => {
	
	return (
		<div className="flex flex-col gap-[10px]">
			{selectedDay?.slots && selectedDay.slots.map((slot:TimeSlot) => {
				return 	<div
									className={`time_slot ${selectedSlot?.id === slot.id ? 'time_slot--active' : ''}`}
									key={slot.id}
									onClick={() => setSelectedSlot(slot)}
								>
									<div className="font-semibold">
										{new Date(slot.startTime).toLocaleTimeString('en',
											{ timeStyle: 'short', hour12: false, timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone })
										}
									</div>
									<div className="mr-auto text-[#4D5461]">{slot.duration} min</div>
									<div className="font-semibold text-[#4D5461]">&#8364;{slot.price}</div>
								</div>
			})}
		</div>
	)
}

export default TimeSlots