/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/app/client/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}